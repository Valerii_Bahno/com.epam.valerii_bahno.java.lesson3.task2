import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {

        Addition addition = new Addition();
        Subtraction subtraction = new Subtraction();
        Multiplication multiplication = new Multiplication();
        Division division = new Division();
        Exponentiation exponentiation = new Exponentiation();
        Root root = new Root();

        Scanner in = new Scanner(System.in);
        /* Enter integer values A and B from the keyboard */
        System.out.println("Enter value A:");
        int a = in.nextInt();
        System.out.println("Enter value B (B > 0):");
        int b = in.nextInt();
        /* Enter the operation from the keyboard (add, subtract, multi, div) */
        System.out.println("Enter operation: " +
                "add / subtract / multiplic / div / exponent / sqrt");
        String c = in.next();

        /* Operations with values A and B */
        switch (c) {
            case "add":
                addition.operation(a, b);
                break;
            case "subtract":
                subtraction.operation(a, b);
                break;
            case "multiplic":
                multiplication.operation(a, b);
                break;
            case "div":
                division.operation(a, b);
                break;
            case "exponent":
                exponentiation.operation(a, b);
                break;
            case "sqrt":
                root.operation(a, b);
                break;
            default:
                System.out.println("You entered wrong operation!");
        }


    }

}
