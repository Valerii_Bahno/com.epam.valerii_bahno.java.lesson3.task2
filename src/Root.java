public class Root extends Operation {

    @Override
    void operation(int a, int b) {
        double sqrt1 = Math.sqrt(a);
        double sqrt2 = Math.sqrt(b);

        if (a >= 0 && b >= 0) {
            System.out.println("Root of value a = " + sqrt1);
            System.out.println("Root of value b = " + sqrt2);
        }
        else {
            System.out.println("Number a or b is less than 0!");
        }
    }
}
