public class Subtraction extends Operation {

    @Override
    void operation(int a, int b) {
        int subtract = a - b;
        System.out.println("Difference of values = " + subtract);
    }
}
