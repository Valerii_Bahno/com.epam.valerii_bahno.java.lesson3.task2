public class Addition extends Operation {

    @Override
    void operation(int a, int b) {
        int sum = a + b;
        System.out.println("Sum of values = " + sum);
    }
}
