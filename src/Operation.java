abstract class Operation {
    abstract void operation(int a, int b);
}
