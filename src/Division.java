public class Division extends Operation{

    @Override
    void operation(int a, int b) {
        if (b == 0) {
            System.out.println("B = 0 - division is impossible");
        }
        else {
            int div = a / b;
            System.out.println("Division of values = " + div);
        }
    }
}
