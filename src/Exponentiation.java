public class Exponentiation extends Operation{

    @Override
    void operation(int a, int b) {
        double exponent = Math.pow(a, b);
        System.out.println("Raising a number to a power = " + exponent);
    }
}
